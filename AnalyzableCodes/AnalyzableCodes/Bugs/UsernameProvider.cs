﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalyzableCodes.Bugs
{
    class UsernameProvider
    {
        public readonly List<string> roles;

        private string username;
        private string fullname;
        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }


        public UsernameProvider()
        {

        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public string Fullname
        {
            get { return username; }
            set { username = value; }
        }

        public void WalkInRoles(string what)
        {
            var target = roles.Where(p => p.Equals(what)).FirstOrDefault();
            WalkInRoles(target);            
        }

    }
}
